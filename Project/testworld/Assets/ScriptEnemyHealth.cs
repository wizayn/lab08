﻿using UnityEngine;
using System.Collections;

public class ScriptEnemyHealth : MonoBehaviour {
    public int enemyHealth = 5;

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Bullet")
        {
            enemyHealth--;
        }

        if (enemyHealth <= 0)
        {
            Destroy(gameObject);
        }
    }
}
