﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

    public GameObject spawnPoint;
    public GameObject[] enemiesToSpawn;

    void OnCollisionEnter(Collision col)
    {
        Debug.Log("Collision Detected");
        if (col.gameObject.tag == "Player")
        {
            Debug.Log("Collision with " + col);
            foreach (GameObject enemy in enemiesToSpawn)
            {
                GameObject spawnedEnemy = Instantiate(enemy, spawnPoint.transform.position, transform.rotation) as GameObject;
            }
        }
    }


}
