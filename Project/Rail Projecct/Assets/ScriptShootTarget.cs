﻿using UnityEngine;
using System.Collections;

public class ScriptShootTarget : MonoBehaviour {
    public Rigidbody bullet;
    
    public float speed = 5.0f;
    public float fireRate = 0.5f;
    private float nextFire = 0.0f;
 
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        Rigidbody clone;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        
        if (Input.GetMouseButton(0) && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            clone = Instantiate(bullet, transform.position, transform.rotation) as Rigidbody;
            clone.velocity = ray.direction.normalized * speed;
        }
        Debug.DrawRay(ray.origin, ray.direction * 100f, Color.red);
    }
}
