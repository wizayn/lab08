﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ScriptEnemyMovement
{
    public GameObject enemy;
    public GameObject moveplace;
    public GameObject playerToLookAt;

    public float speed;
    public float waitTime;

    public string name; 
}
