﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class PrefabCreator : EditorWindow
{
    [MenuItem("Tools/Create Engine")]
    static void Init()
    {
        GameObject spawn = new GameObject("Engine");
        spawn.AddComponent<Engine>();
        spawn.AddComponent<Gizmodos>();
    }


}
