﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class ServerConnection : MonoBehaviour
{
    int serverSocketID = -1;
    int maxConnections = 10;
    byte unreliableChannelID;
    byte reliableChannelID;
    bool serverInitialized = false;



	// Use this for initialization
	void Start ()
    {
        DontDestroyOnLoad(this);

        GlobalConfig globalConfig = new GlobalConfig();
        globalConfig.ReactorModel = ReactorModel.FixRateReactor;
        globalConfig.ThreadAwakeTimeout = 10;

        ConnectionConfig connectionConfig = new ConnectionConfig();
        reliableChannelID = connectionConfig.AddChannel(QosType.ReliableSequenced);
        unreliableChannelID = connectionConfig.AddChannel(QosType.UnreliableSequenced);

        HostTopology hostTopology = new HostTopology(connectionConfig, maxConnections);
        NetworkTransport.Init(globalConfig);

        serverSocketID = NetworkTransport.AddHost(hostTopology, 7777);

        if(serverSocketID < 0)
        {
            Debug.Log("Server socket creation failed!");
        }
        else
        {
            Debug.Log("Server socket creation success");
        }

        serverInitialized = true;
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if(!serverInitialized)
        {
            return;
        }

        int recHostId;                          //Who receiving the message
        int connectionId;                       //Who sent the message
        int channelID;                          //What channel the message was sent from
        int dataSize;                           //How large the message can be
        byte[] buffer = new byte[1024];         //the actual message
        byte error;                             //if there is an error

        NetworkEventType networkEvent = NetworkEventType.DataEvent;

        do
        {
            networkEvent = NetworkTransport.Receive(out recHostId, out connectionId, out channelID, buffer, 1024, out dataSize, out error);

            switch(networkEvent)
            {
                case NetworkEventType.Nothing:
                    break;
                case NetworkEventType.ConnectEvent:
                    //Server recieved "dis"?connect event
                    if(recHostId == serverSocketID)
                    {
                        Debug.Log("Server: Player " + connectionId.ToString() + " connected!");
                    }
                    break;
                case NetworkEventType.DataEvent:
                    if(recHostId == serverSocketID)         //verify the server is the intended target
                    {
                        //Open a memory stream with a size equal to the buffer we set up earlier
                        Stream memoryStream = new MemoryStream(buffer);

                        //Create a binary formatter to begin reading the information from the memory stream
                        BinaryFormatter binaryFormatter = new BinaryFormatter();

                        //Utilize the binary formatter to deserialize the binary information stored in the memory string then convert that into a string
                        string message = binaryFormatter.Deserialize(memoryStream).ToString();

                        //debug out the message you worked so hard to figure out!
                        Debug.Log("Server: Recieved Data from " + connectionId.ToString() + "! Message: " + message);

                        RespondMessage(message, connectionId); //who send the message so the server can respond to the specific person
                    }
                    break;
                case NetworkEventType.DisconnectEvent:
                    //server received disconnect event
                    if(recHostId == serverSocketID)
                    {
                        Debug.Log("Server: Recieved disconnect from " + connectionId.ToString());
                    }
                    break;
            }

        } while (networkEvent != NetworkEventType.Nothing);

        
	}

    void SendMessage(string message, int target)
    {
        byte error;
        byte[] buffer = new byte[1024];
        Stream memoryStream = new MemoryStream(buffer);
        BinaryFormatter binaryFormatter = new BinaryFormatter();

        binaryFormatter.Serialize(memoryStream, message);

        //Who is sending, where to, what channel, what info, how much info, if there is an error
        NetworkTransport.Send(serverSocketID, target, reliableChannelID, buffer, (int)memoryStream.Position, out error);

        if(error != (byte)NetworkError.Ok)      //error is always assigned, and it uses Ok to notate that there is nothing wrong
        {
            NetworkError networkError = (NetworkError)error;
            Debug.Log("Error: " + networkError.ToString());
        }
    }

    void RespondMessage(string message, int playerID)
    {
        
        if(message == "FirstConnect")
        {
            Debug.Log("First connect message received from player: " + playerID);
            SendMessage("goto_NewScene", playerID);
        }

        if(SceneManager.GetActiveScene().name != "Scene2")
        {
            SceneManager.LoadScene("Scene2");
            //NetworkEventType.DataEvent = 
        }
        
    }
}
